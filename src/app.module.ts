import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { CartModule } from './modules';
import { DatabaseModule } from './infrastructure';

@Module({
  imports: [
    ConfigModule.forRoot({
      //envFilePath: ['.env'],
      isGlobal: true,
    }),
    DatabaseModule,
    CartModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
