// cart.service.spec.ts
import { Test, TestingModule } from '@nestjs/testing';
import { CartService } from './cart.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CartEntity } from '../../infrastructure/database/entities/cart.entity';
import { CartItemEntity } from '../../infrastructure/database/entities/cart_item.entity';
import { cartProviders } from './cart.providers';

describe('CartService', () => {
  let service: CartService;
  let cartRepository: Repository<CartEntity>;
  let cartItemRepository: Repository<CartItemEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CartService,
        {
          provide: getRepositoryToken(CartEntity),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(CartItemEntity),
          useClass: Repository,
        },
        ...cartProviders,
      ],
    }).compile();

    service = module.get<CartService>(CartService);
    cartRepository = module.get<Repository<CartEntity>>(getRepositoryToken(CartEntity));
    cartItemRepository = module.get<Repository<CartItemEntity>>(getRepositoryToken(CartItemEntity));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  // Mocks
  const mockCart: CartEntity = {
    id: '1',
    items: [],
  };

  const mockCartItem: CartItemEntity = {
    productId: 'product1',
    cartId: '1', // Agrega el cartId correspondiente
    quantity: 2,
    cart: mockCart,
  };

  // Casos de prueba
  it('should get a cart by id', async () => {
    jest.spyOn(cartRepository, 'findOne').mockResolvedValueOnce(mockCart);

    const result = await service.getCart('1');

    expect(result).toEqual({
      id: '1',
      items: [],
    });
  });

  it('should add a cart item', async () => {
    jest.spyOn(cartRepository, 'findOne').mockResolvedValueOnce(mockCart);
    jest.spyOn(cartItemRepository, 'create').mockReturnValueOnce(mockCartItem);
    jest.spyOn(cartItemRepository, 'save').mockResolvedValueOnce(mockCartItem);

    const result = await service.addCartItem('1', 'product1', 2);

    expect(result).toEqual({
      id: '1',
      items: [
        {
          productId: 'product1',
          quantity: 2,
        },
      ],
    });
  });

  it('should delete a cart item', async () => {
    jest.spyOn(cartRepository, 'findOne').mockResolvedValueOnce(mockCart);
    jest.spyOn(cartItemRepository, 'remove').mockResolvedValueOnce(undefined);

    const result = await service.deleteCartItem('1', 'product1', 2);

    expect(result).toEqual({
      id: '1',
      items: [
        {
          productId: 'product1',
          quantity: 1,
        },
      ],
    });
  });
});
