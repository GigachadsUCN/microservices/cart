import { DataSource } from 'typeorm';
import { CartEntity, CartItemEntity } from '../../infrastructure/database/entities';

export const cartProviders = [
  {
    provide: 'CART_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(CartEntity),
    inject: ['DATA_SOURCE'],
  },
  {
    provide: 'CART_ITEM_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(CartItemEntity),
    inject: ['DATA_SOURCE'],
  },
];
