import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CartEntity } from '../../infrastructure/database/entities/cart.entity';
import { CartItemEntity } from '../../infrastructure/database/entities/cart_item.entity';
import { Cart, CartItem } from '../../interfaces/cart.interface';

@Injectable()
export class CartService {
  constructor(
      @Inject('CART_REPOSITORY')
      private cartRepository: Repository<CartEntity>,
      @Inject('CART_ITEM_REPOSITORY')
      private cartItemRepository: Repository<CartItemEntity>,
  ) { }

  async getCart(cartId: string): Promise<Cart> {
    const cart = await this.cartRepository.findOne({ where: { id: cartId }, relations: ['items'] });
    return this.convertToCartDto(cart);
  }

  async addCartItem(cartId: string, productId: string, quantity: number): Promise<Cart> {
    // Verificar si el carrito existe
    let cart = await this.cartRepository.findOne({ where: { id: cartId }, relations: ['items'] });

    // Si el carrito no existe, crear uno nuevo
    if (!cart) {
      cart = this.cartRepository.create({ id: cartId, items: [] });
      await this.cartRepository.save(cart);
    }

    // Verificar si el producto ya está en el carrito
    const existingItem = cart.items.find(item => item.productId === productId);

    if (existingItem) {
      existingItem.quantity += quantity;
      await this.cartItemRepository.save(existingItem);
    } else {
      const newItem = this.cartItemRepository.create({
        productId: productId,
        quantity: quantity,
        cartId: cartId,
      });
      cart.items.push(newItem);
      await this.cartItemRepository.save(newItem);
    }

    return this.convertToCartDto(cart);
  }

  async deleteCartItem(cartId: string, productId: string, quantity: number): Promise<Cart> {
    const cart = await this.cartRepository.findOne({ where: { id: cartId }, relations: ['items'] });

    const itemIndex = cart.items.findIndex(item => item.productId === productId);
    const cartItem = cart.items[itemIndex];

    if (itemIndex === -1) {
      throw new HttpException('Cart item not found', HttpStatus.NOT_FOUND);
    }

    cartItem.quantity -= quantity;

    if (cartItem.quantity <= 0) {
      await this.cartItemRepository.remove(cartItem);
      cart.items.splice(itemIndex, 1);
    } else {
      await this.cartItemRepository.save(cartItem);
    }

    await this.cartRepository.save(cart);

    return this.convertToCartDto(cart);
  }

  private convertToCartDto(cart: CartEntity): Cart {
    return {
      id: cart.id,
      items: cart.items.map(item => ({ productId: item.productId, quantity: item.quantity })),
    };
  }
}
