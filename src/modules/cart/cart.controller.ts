import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { CartService } from './cart.service';
import { CartMsg } from '../../utils/constants';

@Controller()
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @MessagePattern(CartMsg.GET)
  async getCart(@Payload() cartId: string): Promise<any> {
    return this.cartService.getCart(cartId);
  }

  @MessagePattern(CartMsg.ADD)
  async addCartItem(@Payload() data: { cartId: string, productId: string, quantity: number }): Promise<any> {
    const { cartId, productId, quantity } = data;
    return this.cartService.addCartItem(cartId, productId, quantity);
  }

  @MessagePattern(CartMsg.DELETE)
  async deleteCartItem(@Payload() data: { cartId: string, productId: string, quantity: number }): Promise<any> {
    const { cartId, productId, quantity } = data;
    return this.cartService.deleteCartItem(cartId, productId, quantity);
  }
}
