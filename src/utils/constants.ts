export enum RabbitMQ {
  CartQueue = 'cart',
}

export enum CartMsg {
  GET = 'GET_CART',
  ADD = 'ADD_ITEM',
  DELETE = 'DELETE_ITEM',
}
