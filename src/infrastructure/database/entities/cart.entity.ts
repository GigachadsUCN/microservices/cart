import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { CartItemEntity } from './cart_item.entity';

@Entity('cart')
export class CartEntity {
  @PrimaryColumn({ type: 'varchar' })
  id: string;

  @OneToMany(() => CartItemEntity, item => item.cart)
  items: CartItemEntity[];
}
