import { Entity, Column, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';
import { CartEntity } from './cart.entity';

@Entity('cart_item')
export class CartItemEntity {
  @PrimaryColumn({ type: 'varchar', name: 'product_id' })
  productId: string;

  @PrimaryColumn({ type: 'varchar', name: 'cart_id' })
  cartId: string;

  @Column({ type: 'integer' })
  quantity: number;

  @ManyToOne(() => CartEntity, cart => cart.items)
  @JoinColumn({ name: 'cart_id', referencedColumnName: 'id' })
  cart: CartEntity;
}
