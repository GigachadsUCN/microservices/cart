export interface Cart {
    id: string;
    items: CartItem[];
}

export interface CartItem {
    productId: string;
    quantity: number;
}
