CREATE TABLE cart (
    id VARCHAR PRIMARY KEY
);

CREATE TABLE cart_item (
    product_id VARCHAR NOT NULL,
    cart_id VARCHAR NOT NULL,
    quantity INTEGER NOT NULL,
    PRIMARY KEY (product_id, cart_id),
    CONSTRAINT fk_cart_item_cart FOREIGN KEY (cart_id) REFERENCES cart(id) ON DELETE CASCADE
);
